
var z = require('zeninode')();

var async = require('async');
var AWS = require('aws-sdk');

function DynamoDBTuner(configPath) {
	AWS.config.loadFromPath(configPath);
	this.dynamodb = new AWS.DynamoDB({apiVersion:'2012-08-10'});
}

DynamoDBTuner.prototype.getStatus = function(tableName, cb) {
	z.fnlog(arguments);
	this.dynamodb.describeTable({TableName:tableName}, cb);
}

DynamoDBTuner.prototype.waitForActive = function(tableName, cb) {
	var _this = this;
	_this.getStatus(tableName, function(err, data) {
		if(err) cb(err);
		else {
			if(data.Table.TableStatus !== 'ACTIVE') {
				setTimeout(function(){
					_this.waitForActive(tableName, cb);
				}, 5000);
			} else {
				cb(null, data);
			}
		}
	});
}

DynamoDBTuner.prototype.updateReadThrouput = function(tableName, targetReadThrouput, cb) {
	var _this = this;
	async.waterfall(z.async.task([
	function(cb) {
		_this.waitForActive(tableName, cb);
	}, function(cb, data) {
		var currThrouput = data.Table.ProvisionedThroughput;
		var param = {TableName:tableName, ProvisionedThroughput:{WriteCapacityUnits:currThrouput.WriteCapacityUnits}};
		if(currThrouput.ReadCapacityUnits*2 < targetReadThrouput) {
			param.ProvisionedThroughput.ReadCapacityUnits = currThrouput.ReadCapacityUnits*2;
		} else if(currThrouput.ReadCapacityUnits !== targetReadThrouput){
			param.ProvisionedThroughput.ReadCapacityUnits = targetReadThrouput;
		} else {
			param = null;
		}
		if(param) {
			_this.dynamodb.updateTable(param, function(err) {
				if(err) cb(err);
				else _this.updateReadThrouput(tableName, targetReadThrouput, cb);
			});
		} else {
			cb();
		}
	}
	]), cb);
}

DynamoDBTuner.prototype.updateWriteThrouput = function(tableName, targetWriteThrouput, cb) {
	var _this = this;
	async.waterfall(z.async.task([
	function(cb) {
		_this.waitForActive(tableName, cb);
	}, function(cb, data) {
		var currThrouput = data.Table.ProvisionedThroughput;
		var param = {TableName:tableName, ProvisionedThroughput:{ReadCapacityUnits:currThrouput.ReadCapacityUnits}};
		if(currThrouput.WriteCapacityUnits*2 < targetWriteThrouput) {
			param.ProvisionedThroughput.WriteCapacityUnits = currThrouput.WriteCapacityUnits*2;
		} else if(currThrouput.WriteCapacityUnits !== targetWriteThrouput){
			param.ProvisionedThroughput.WriteCapacityUnits = targetWriteThrouput;
		} else {
			param = null;
		}
		if(param) {
			this.dynamodb.updateTable(param, function(err) {
				if(err) cb(err);
				else _this.updateWriteThrouput(tableName, targetWriteThrouput, cb);
			});
		} else {
			cb();
		}
	}
	]), cb);
}

DynamoDBTuner.prototype.getTableList = function(cb) {
	this.dynamodb.listTables({}, cb);
}

module.exports = {
	DynamoDBTuner:DynamoDBTuner
};

